---
title: Affordability Indices
draft: false
menu: main
weight: 10
bannerHeading: Affordability Indices
bannerText: >
  The future of transportation includes implementing goals and projects, securing funding, overcoming obstacles, and staying on the cusp of public needs and pioneering ideas.
---
## LRTP Affordability Indices

### As the metropolitan planning area grows smartly and steadily, the transportation system will allow for improved quality of life, reduced disparities, reduced dependence on single-occupancy vehicles, and the reliable and safe movement of people and goods.

The combined input about the current and future transportation system conveyed
strong public support about how our area transportation system should evolve: 
a more environmentally sustainable transportation system, additional pedestrian 
and bicycle infrastructure, shorter transit times, equitable access to transportation
services, and a compact urban area that supports active transportation and limits 
fringe development. Select the sections in the side menu to learn more about how 
CUUATS staff, member agencies, and community members will bring this vision to fruition.
