---
title: Home
draft: false
hide_edit_btn: true
bannerHeading: "LRTP 2050"
bannerText: >
  As a community transportation policy document, the LRTP provides a regional
  transportation vision for 2050 to guide future transportation
  investments.
---

<p float="left">
  <img src="Color_Logo_Only.png" width="100%" alt="Champaign County Logo" />
</p>

<style>
  h1 {
    border: solid;
    background-color: #ffffff;
  }
  p {
    text-align: left;
    padding-left: 100px;
    padding-right: 100px;
  }
  h4 {
    text-align: center;
    padding-left: 40px;
    padding-right: 40px;
  }
</style>

<h1 class="desktop-heading">Welcome to the Champaign County Housing and Transportation Affordability Index.</h1>
<h1 class="mobile-heading">HTAA</h1>

<h2>The Champaign-Urbana Urban Area Transportation Study (CUUATS) presents the 5-year LRTP, which covers the Metropolitan Planning Area including Champaign, Urbana, Savoy, Mahomet, Tolono, Bondville, the University of Illinois campus, and the Champaign-Urbana Mass Transit District service area.</h2>

<hr style="border: none; width: 100%; border-top: 3px solid black;">

<h1 class="desktop-heading">Announcements</h1>
<h1 class="mobile-heading">Announcements</h1>

<h1>LRTP 2050 approved at CUUATS Committee Meetings in December 2024</h1>
<h2>The LRTP 2050 was approved on December 4 by the CUUATS Technical Committee and on December 11, 2024 by the CUUATS Policy Committee. This website serves as the documentation of the plan.</h2>

<!-- <a target="new" href="https://ccrpc.gitlab.io/transportation-voices-2050/" class="btn-custom btn-block w-lg-50 mx-auto">Click here to access the map</a> -->

<h3 style="text-align: left;"><u><a href="Tech-Resolution-LRTP2050-Approval.pdf">CUUATS Technical Committee Resolution of Approval, December 4, 2024</a></u></h3>
<h3 style="text-align: left;"><u><a href="Policy-Resolution-LRTP2050-Approval.pdf">CUUATS Policy Committee Resolution of Approval, December 11, 2024</a></u></h3>

<hr style="border: none; width: 100%; border-top: 3px solid black;">

<h1 class="desktop-heading">We're Transporting and Transforming the Champaign-Urbana Region!</h1>
<h1 class="mobile-heading">We're Transporting and Transforming the C-U Region!</h1>

<center><h3><u>More About This Website</u></h3></center>

Use the navigation bar at the top of the page to review the draft web-based document.

The [LRTP 2050 Updates section](https://ccrpc.gitlab.io/lrtp-2050/overview/updates/) provides current information about the status of the document's development.

Some maps and images are large files that take time to load.